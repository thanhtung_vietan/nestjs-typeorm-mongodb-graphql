import { Injectable } from '@nestjs/common'
import { PubSub } from 'apollo-server-fastify'

import { ResolverClass } from '@utils'

const publisher = new PubSub()

@Injectable()
export class AbstractClass extends ResolverClass {
  publishNotification = async ({ type, code, data, pubsubCreatedBy, receiver }: any) =>
    this.publisher.publish(type, {
      code,
      data,
      pubsubCreatedBy,
      receiver
    })

  publisher = publisher

  NOTIFICATION_CODE = {
    // NOTE: Calling
    INCOMING: 'INCOMING',
    ACCEPT: 'ACCEPT',
    DENIED: 'DENIED',
    SUCCESS: 'SUCCESS',
    MISSING: 'MISSING'

    // NOTE: Messages
  }

  PUBSUB = {
    _CALL: 'CALL'
  }
}
