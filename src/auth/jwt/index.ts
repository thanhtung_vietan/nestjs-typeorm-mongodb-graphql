import { sign, verify } from 'jsonwebtoken'
import { AuthenticationError, ForbiddenError } from 'apollo-server-fastify'
import { getMongoRepository } from 'typeorm'

import { User } from '@entities'
import { LoginResponse } from '@src/graphql.schema'
import { _appConfigs } from '@src/constants'

type TokenType = 'accessToken' | 'refreshToken' | 'emailToken' | 'resetPassToken'

const common = {
  accessToken: {
    privateKey: _appConfigs.ACCESS_TOKEN_SECRET,
    signOptions: {
      expiresIn: '30d' // 15m
    }
  },
  refreshToken: {
    privateKey: _appConfigs.REFRESH_TOKEN_SECRET!,
    signOptions: {
      expiresIn: '7d' // 7d
    }
  },
  emailToken: {
    privateKey: _appConfigs.EMAIL_TOKEN_SECRET!,
    signOptions: {
      expiresIn: '1d' // 1d
    }
  },
  resetPassToken: {
    privateKey: _appConfigs.RESETPASS_TOKEN_SECRET!,
    signOptions: {
      expiresIn: '1d' // 1d
    }
  }
}

export const generateToken = async (user: User, type: TokenType): Promise<string> => {
  return await sign(
    {
      _id: user._id
    },
    common[type].privateKey,
    {
      issuer: _appConfigs.ISSUER!,
      subject: user.email,
      audience: _appConfigs.AUDIENCE!,
      algorithm: 'HS256',
      expiresIn: common[type].signOptions.expiresIn
    }
  )
}

export const verifyToken = async (token: string, type: TokenType): Promise<User> => {
  let currentUser

  await verify(token, common[type].privateKey, async (err, data: any) => {
    if (err) {
      throw new AuthenticationError('Authentication token is invalid, please try again.')
    }

    currentUser = await getMongoRepository(User).findOne({ _id: data._id })
  })

  return currentUser
}

export const tradeToken = async (user: User): Promise<LoginResponse> => {
  if (!user.isActive) {
    throw new ForbiddenError('User already doesnt exist.')
  }

  if (user.isLocked) {
    throw new ForbiddenError('Your email has been locked.')
  }

  const token = await generateToken(user, 'accessToken')

  return { token }
}
