import * as env from 'dotenv'
env.config()

import { NestFactory } from '@nestjs/core'
import {
  FastifyAdapter,
  NestFastifyApplication,
} from '@nestjs/platform-fastify';
import { json, urlencoded } from 'body-parser'
import { express as voyagerMiddleware } from 'graphql-voyager/middleware'

import { ApplicationModule } from './app.module'
import chalk from 'chalk'

import { _appConfigs } from '@constants'

const { DEVELOPMENT } = _appConfigs

const bootstrap = async () => {
  const { PORT = _appConfigs.DEFAULT_PORT, NODE_ENV = DEVELOPMENT } = process.env

  const app = await NestFactory.create<NestFastifyApplication>(ApplicationModule, new FastifyAdapter())

  if (NODE_ENV === DEVELOPMENT) {
    app.use(_appConfigs.SCHEMA_PATH, voyagerMiddleware({ endpointUrl: _appConfigs.GRAPHQL_PATH }))
  }

  app.enableCors({ origin: '*' })
  app.use(json({ limit: '10mb' }))
  app.use(urlencoded({ limit: '10mb', extended: true }))

  await app.listen(Number(PORT))

  // NOTE: HMR
  if (module.hot) {
    module.hot.accept()
    module.hot.dispose(() => app.close())
  }

  console.log(`------- Server ready at port: ${chalk.greenBright(`${PORT}`)} -------`)
}

bootstrap()
